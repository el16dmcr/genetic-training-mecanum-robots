class Goal{
    constructor(){
        this.size = 50;
        this.obstaclePadding = 30
        this.pos = createVector();
        this.goalQuadIndex = 0;
        this.newRandomLocation();
        
    
    }
    
    incrementGoalQuadIndex(){
        this.goalQuadIndex ++;
    }
    
    resetGoalQuadIndex(){
        this.goalQuadIndex = 0;
    }
    
    newRandomLocation(){
        let validPos = false;
        var newPos;
        while(validPos == false){
            newPos = this.generateOuterPerimeterRandomPos(this.goalQuadIndex);
            if(this.checkPos(newPos) == true){
                validPos = true;
            }
        }
        this.pos = newPos;
    }
    
    generateOuterPerimeterRandomPos(goalQuadIndex){
        var outerPerimeterPos = createVector();
        if(goalQuadIndex == 0){
            outerPerimeterPos.x = random(width);
            outerPerimeterPos.y = random(300);
//            outerPerimeterPos.x = width/2;
//            outerPerimeterPos.y = 100;
            
        }
        else if(goalQuadIndex == 1){
            outerPerimeterPos.x = random(width - 300, width);
            outerPerimeterPos.y = random(height);
//            outerPerimeterPos.x = width - 100;
//            outerPerimeterPos.y = height/2;
        }
        else if(goalQuadIndex == 2){
            outerPerimeterPos.x = random(width);
            outerPerimeterPos.y = random(height - 300, height);
//            outerPerimeterPos.x = width/2;
//            outerPerimeterPos.y = height - 100;
        }
        else{
            outerPerimeterPos.x = random(300);
            outerPerimeterPos.y = random(height);
//            outerPerimeterPos.x = 100;
//            outerPerimeterPos.y = height/2;
        }
        
        
        return outerPerimeterPos;
    }
    
    checkPos(position){
        var validPos = true 
        for(var i = 0; i < world.obstacles.length; i++){
            if(world.obstacles[i].checkCollisionWithObstacle(position.x, position.y, this.obstaclePadding, this.size/2) == true){
                validPos = false;
            }
        }
        return validPos;
    }
    
    show(){
        push();
        noStroke();
        fill(0, 255, 0, 100);
        ellipse(this.pos.x, this.pos.y, this.size, this.size);
        
        
        pop();
    }
    
}