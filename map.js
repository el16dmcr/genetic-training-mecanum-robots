class TestWorld{
    constructor(){
        var standardWallWidth_m = 0.4;
        this.obstacles = [];
        this.obstacles.push(new Obstacle(mapWidth_m/2, standardWallWidth_m/2, mapWidth_m, standardWallWidth_m, false));
        this.obstacles.push(new Obstacle(mapWidth_m - standardWallWidth_m/2, mapHeight_m/2, mapHeight_m, standardWallWidth_m, true));
        this.obstacles.push(new Obstacle(mapWidth_m/2, mapHeight_m - standardWallWidth_m/2, mapWidth_m, standardWallWidth_m, false));
        this.obstacles.push(new Obstacle(standardWallWidth_m/2, mapHeight_m/2, mapHeight_m, standardWallWidth_m, true));
        
        
        
        
    }
    
    createObstacles(){
        var innerBoxSize_m = 8
        var standardWallWidth_m = 0.4;
        this.obstacles.push(new Obstacle(mapWidth_m/2, mapHeight_m/2 - innerBoxSize_m/2, 4, standardWallWidth_m, false));
        this.obstacles.push(new Obstacle(mapWidth_m/2 + innerBoxSize_m/2, mapHeight_m/2, 4, standardWallWidth_m, true));
        this.obstacles.push(new Obstacle(mapWidth_m/2, mapHeight_m/2 + innerBoxSize_m/2, 4, standardWallWidth_m, false));
        this.obstacles.push(new Obstacle(mapWidth_m/2 - innerBoxSize_m/2, mapHeight_m/2, 4, standardWallWidth_m, true));
    }
    
    show(){
        for(var i = 0; i < this.obstacles.length; i++){
            this.obstacles[i].show();
        }
    }
}