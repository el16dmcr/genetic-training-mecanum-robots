var lifeP;
var generationP;
var count = 0;
var numberOfObstacles = 0;
var obstacles = [];
var obstacleWidth = 10;

var scalingFactor = 50
var generationNumber = 0;
var turboTraining = true;
var cyclesPerFrame = 1;
var fRate = 60;

var spawnObstacles = false;
var obstaclesSpawned = false;

// middle of the map will be (0,0)
// define the size of the map in metres and then convert that to pixels
var mapWidth_m = 18;
var mapHeight_m = 18;
//var mapMapDistFromOrigin = sqrt(pow(mapHeight_m / 2, 2) + pow(mapWidth_m/2, 2));

var ctx;
var chart;

var ctxAvgFit;
var chartAvgFit;

var ctxGoals;
var chartGoals;

var theoreticalMaxFitness = 20;


var testCounter = 1;

function setup() 
{
  // put setup code here
    var mapWidth_p = mapWidth_m * scalingFactor;
    var mapHeight_p = mapHeight_m * scalingFactor;
    createCanvas(mapWidth_p, mapHeight_p);
    
    
    tf.setBackend('cpu');
    lifeP = createP();
    generationP = createP();
    world = new TestWorld();
    goal = new Goal();
    population = new Population();
    world.obstacles[0].testFunction();
    //createChartNormalFitness();
    //createChartAverageFitness();
    createChartGoalsReached();
    systemTest();
    if(spawnObstacles == true && obstaclesSpawned == false){
        obstaclesSpawned = true;
        world.createObstacles();
    }
    
    frameRate(fRate);

}

function draw() 
{

    process();
    show();
    //print("frame rate is: " + str(frameRate()));
     
}

function createChartNormalFitness(){
    ctx = document.getElementById('chart').getContext('2d');
    chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
            label: 'Best fitness',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(0, 255, 0)',
            borderColor: 'rgb(0, 255, 0)',
            data: []
        }, {
            label: 'Worst fitness',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(255, 0, 0)',
            borderColor: 'rgb(255, 0, 0)',
            data: []
        }, {
            label: 'Mean fitness',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(0, 0, 255)',
            borderColor: 'rgb(0, 0, 255)',
            data: []
            
        }]
//        datasets: [{
//            label: 'Worst fitness',
//            fill: false,
//            backgroundColor: 'rgb(255, 0, 0)',
//            borderColor: 'rgb(255, 0, 0)',
//            data: []
//        }]
    },

    // Configuration options go here
    options: {}
    });
}

function createChartAverageFitness(){
    ctxAvgFit = document.getElementById('chart').getContext('2d');
    chartAvgFit = new Chart(ctxAvgFit, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
            label: 'Best fitness - 7 generation average',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(0, 255, 0)',
            borderColor: 'rgb(0, 255, 0)',
            data: []
        }, {
            label: 'Worst fitness - 7 generation average',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(255, 0, 0)',
            borderColor: 'rgb(255, 0, 0)',
            data: []
        }, {
            label: 'Mean fitness - 7 generation average',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(0, 0, 255)',
            borderColor: 'rgb(0, 0, 255)',
            data: []
            
        }]
//        datasets: [{
//            label: 'Worst fitness',
//            fill: false,
//            backgroundColor: 'rgb(255, 0, 0)',
//            borderColor: 'rgb(255, 0, 0)',
//            data: []
//        }]
    },

    // Configuration options go here
    options: {}
    });
}

function createChartGoalsReached(){
    ctxGoals = document.getElementById('chart').getContext('2d');
    chartGoals = new Chart(ctxGoals, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: [],
        datasets: [{
            label: 'Number of goals reached',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgb(0, 255, 0)',
            borderColor: 'rgb(0, 255, 0)',
            data: []
        }]
            
        },
//        datasets: [{
//            label: 'Worst fitness',
//            fill: false,
//            backgroundColor: 'rgb(255, 0, 0)',
//            borderColor: 'rgb(255, 0, 0)',
//            data: []
//        }]

    // Configuration options go here
    options: {}
    });
    
}


function updateChart(){
    //chart.data.labels.push("gen: " + str(generationNumber));
    //chart.data.datasets[0].data.push(population.bestFitnessEachGen[population.bestFitnessEachGen.length - 1]);
    //chart.data.datasets[1].data.push(population.worstFitnessEachGen[population.worstFitnessEachGen.length - 1]);
    //chart.data.datasets[2].data.push(population.meanFitnessEachGen[population.meanFitnessEachGen.length - 1]);
    //chart.update();
//    chartAvgFit.data.labels.push("gen: " + str(generationNumber));
//    chartAvgFit.data.datasets[0].data.push(population.bestFitnessRollingAverage[population.bestFitnessRollingAverage.length - 1]);
//    chartAvgFit.data.datasets[1].data.push(population.worstFitnessRollingAverage[population.worstFitnessRollingAverage.length - 1]);
//    chartAvgFit.data.datasets[2].data.push(population.meanFitnessRollingAverage[population.meanFitnessRollingAverage.length - 1]);
//    chartAvgFit.update();
    
    chartGoals.data.labels.push("gen: " + str(generationNumber));
    chartGoals.data.datasets[0].data.push(population.numGoalsReachedPerGen[population.numGoalsReachedPerGen.length - 1]);
    chartGoals.update();
    testCounter++;
}

function process(){
    for(let n = 0; n < cyclesPerFrame; n++){
        population.run();
        if(population.checkIfGenerationComplete() == true){
            goal.resetGoalQuadIndex();
            goal.newRandomLocation();
            population.handleNextGeneration();
            if(spawnObstacles == true && obstaclesSpawned == false){
                obstaclesSpawned = true;
                world.createObstacles();
            }
            generationNumber ++;
            updateChart();
        }
    }

}


function show(){
    background(0);
    
    population.display();
    world.show();
    goal.show();
    push();
    textSize(20);
    textAlign(RIGHT)
    fill(255, 255, 255)
    noStroke();
    text("gen: " + str(generationNumber), width - 30, 40)
    textAlign(LEFT)
    var roundedBestFit = round(population.bestFitnessAllTime, 3);
    text("best fit: " + str(roundedBestFit), 30, 40);
    pop();
    
}

function keyPressed() {
    if(key == 'w'){
        // speed up training
        if(turboTraining == true){
            cyclesPerFrame = 300;
        }
        else{
            cyclesPerFrame = 1;
        }
        turboTraining = 1 - turboTraining;

    }
    else if(keyCode == DOWN_ARROW){
    }
}



function systemTest(){
    var errorsFound = false;
    var testRobot = new Robot(0.6, 0.544, 11, 1, false, false, 800, false);
    
    print("start of test bad progression")
    if(testBadProgressionCounter(testRobot) == true){
        errorsFound = true;
    }
    print("start of test good orientation.")
    if(testGoodOrientationCounter(testRobot) == true){
        errorsFound = true;
    }

    
    testFitnessCalc(testRobot)
    
    
    if(errorsFound == true){
        print("system errors found, pasuing")
    }
    return errorsFound;
    
}

function testFitnessCalc(testRobot){
    testRobot.distToGoalEachEpisode = [0, 0, 0, 0];
    //testRobot.badProgressionCounter = [400, 400, 400, 400];
    testRobot.robotGoodOrientationCounter = [800, 800, 800, 800];
    testRobot.robotAliveCounter = [800, 800, 800, 800];
    testRobot.reachedOuterRing = [true, true, true, true];
    testRobot.distToGoalPerCounterPerEpisode = [[0, 0],[0, 0],[0, 0],[0, 0]];
    testRobot.goalReachedEpisode = [true, true, true, true];
    
    
    testRobot.calcFitness()
    theoreticalMaxFitness = testRobot.fitness;
    //theoreticalMaxFitness = 30;
    print("test robots fitness would be: " + str(testRobot.fitness))
}

function testBadProgressionCounter(testRobot){
    var errorsFound = false;
    var result;
    var expected;
    var testNumber = 0;
    
    var robotHeading = 0;
    var world_goalHeading = -PI;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = -PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = 0;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI - 0.1;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = -PI/2;
    world_goalHeading = -PI;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = -PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = 0;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI - 0.1;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = PI/2;
    world_goalHeading = -PI;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = -PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = 0;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI - 0.1;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = PI;
    world_goalHeading = -PI;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = -PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = 0;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI - 0.1;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = -PI - 0.1;
    world_goalHeading = -PI;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = -PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = 0;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI/2;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    world_goalHeading = PI - 0.1;
    result = testRobot.checkProgressionToGoal(robotHeading, world_goalHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
}

function testGoodOrientationCounter(testRobot){
    var errorsFound = false;
    var robotHeading = -PI;
    var result;
    var expected;
    var testNumber = 0;
    
    var robotHeading = -PI - 0.1;
    var robotOrientation = -PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = -PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = 0;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = -PI /2;
    robotOrientation = -PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = -PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = 0;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = 0;
    robotOrientation = -PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = -PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = 0;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = PI /2;
    robotOrientation = -PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = -PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = 0;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    //--------------------------------------------------------------------------------------------
    
    robotHeading = PI;
    robotOrientation = -PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = -PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = 0;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI / 2;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = false;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
    robotOrientation = PI;
    result = testRobot.checkRobotOrientationAndMovement(robotOrientation, robotHeading);
    expected = true;
    if(result != expected){
        print("error found, test: " + str(testNumber));
        errorsFound = true;
    }
    testNumber++;
    
}


