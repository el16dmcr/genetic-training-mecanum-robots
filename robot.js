class Robot{
    constructor(width_m, height_m, numVisionLines, index, bestRobot, curiousRobot, episodeCounter, brain){
        this.index = index;
        this.bestRobotLastGen = bestRobot;
        this.curiousRobot = curiousRobot;
        this.pos = createVector(width/2, height/2);
        this.startingOrientation = -PI/2;
        this.orientation = this.startingOrientation; 
        
        this.width = width_m * scalingFactor;
        this.length = height_m * scalingFactor;
        
        // velocity in ms-1, output from neural net, needs to be converted to size of this world 
        this.vel_ms = createVector(0,0);
        // angular velocity in radians per second, does NOT need to be converted as angles are same in any size dimension of space
        this.angularVel_rad_s = 0;
        
        this.numberOfVisionLines = numVisionLines;
        this.lowerVisionAngle_deg = -150;
        this.higherVisionAngle_deg = 150;
        
        this.angleSpread_rad = map((this.higherVisionAngle_deg - this.lowerVisionAngle_deg) , 0, 360, 0, 2*PI);
        this.startingAngle_rad = map(this.lowerVisionAngle_deg, 0, 360, 0, 2*PI);
        
        this.visionLineMaxDist_m = 100
        this.visionLineMaxDist_p = this.visionLineMaxDist_m * scalingFactor;
        // distance is stored in pixels, needs to be converted back to metres before feeding through neural net
        this.distanceAlongVisionLine_p = [];
        for( var i = 0; i < this.numberOfVisionLines; i++){
            this.distanceAlongVisionLine_p[i] = this.visionLineMaxDist_p;
        }
        
        if(brain){
            this.brain = brain.copy();
        }
        else{
            // vision lines, dist to goal, heading to goal, robot orientation
            this.brain = new NeuralNet(numVisionLines + 3, 11,11, 3);
        }
        
        this.running = true;
        this.crashed = false;
        
        
        this.drawAlpha = 100;
        this.tempRobotCornerPoints = [];
        
        this.fitness = 0;
        this.normalisedFitness = 0;
        
        this.curiosityFitness = 0;
        this.normalisedCuriosityFitness = 0;
        this.distToGoalEachEpisode = [];
        this.distFromOriginEachEpisode = [];
        
        this.currentDist = 0;
        this.lastDist = 0;
        this.badProgressionCounter = [0, 0, 0, 0];
        this.robotGoodOrientationCounter = [0, 0, 0, 0];
        this.robotAliveCounter = [0, 0, 0, 0];
        this.goalReachedEpisode = [false, false, false, false];
        this.robotClosestDist = [2000, 2000, 2000, 2000];
        
        for(var i =0; i < 4; i++){
            this.robotAliveCounter[i] = episodeCounter;
        }
        this.episodeNumber = 0;
        this.robotPosHistory = [];
        this.numRobotPosHistory = 60;
        this.reachedOuterRing = [false, false, false, false];
        this.crashedTracker = [false, false, false, false];
        this.startDistToGoal = [0, 0, 0, 0];
        
        this.distToGoalPerCounterPerEpisode = [[],[],[],[]];
        
        
    } 
    
    resetRobotForEpisode(){
        this.pos.x = width/2;
        this.pos.y = height/2;
        this.running = true;
        this.crashed = false;
        this.orientation = this.startingOrientation; 
        this.currentDist = 0;
        this.lastDist = 0;
        this.episodeNumber ++;
        this.robotPosHistory = [];
        
    }
    
    testFunctionHandleRobot(){
        var magVelocity = 0.5;
        this.orientation = PI/2;
        var robotHeading = PI/2 - 0.2;
        this.angularVel_rad_s = 0;
        var headingToGoal = this.getHeadingToGoal(this.pos, goal.pos);
        this.vel_ms.x = magVelocity * cos(robotHeading);
        this.vel_ms.y = magVelocity * sin(robotHeading);
        if(this.checkProgressionToGoal(robotHeading, headingToGoal) == true){
            this.badProgressionCounter[this.episodeNumber]++;
        }
        if(this.checkRobotOrientationAndMovement(this.orientation, robotHeading) == true){
            this.robotGoodOrientationCounter[this.episodeNumber]++;
            print("good orientation");
        }
        this.updateMovement();
        
    }
    
    handleRobot(){
        if( this.running == true){
            var headingToGoal = this.getHeadingToGoal(this.pos, goal.pos);
            var robotHeading = this.processInputs(headingToGoal);
            this.updateMovement();
            
            this.storeNewLocation(this.pos);
            this.storeDistToGoal(this.pos, goal.pos);
            if(this.checkRobotPosHistoryForStuck(this.robotPosHistory) == true){
                //print("robot: " + this.index + " stuck");
                this.crashed = true;
                this.crashedTracker[this.episodeNumber] = true;
            }
            if(this.checkProgressionToGoal(robotHeading, headingToGoal) == true){
                this.badProgressionCounter[this.episodeNumber]++;
            }
            if(this.checkRobotOrientationAndMovement(this.orientation, robotHeading) == true){
               this.robotGoodOrientationCounter[this.episodeNumber]++;
            }
            if(this.checkIfRobotInOuterRing() == true){
                this.reachedOuterRing[this.episodeNumber] = true;
                //print("robot reached correct outer quadrant");
            }
            var terminated = this.checkForTermination();
            if(terminated == true){
                this.terminateRobot();
            }
        }
        
        
    }
    
    storeDistToGoal(newPos, goalPos){
        var distToGoal = this.distBetweenTwoPoints(newPos, goalPos);
        var distProportion = distToGoal / this.startDistToGoal[this.episodeNumber];
        this.distToGoalPerCounterPerEpisode[this.episodeNumber].push(distProportion);
        
    }
    
    storeNewLocation(newPos){
        
        if(this.robotPosHistory.length >= this.numRobotPosHistory){
            this.robotPosHistory.shift();
            //print("length maxed")
        }
        var _newPos = createVector();
        _newPos.x = newPos.x;
        _newPos.y = newPos.y;
        this.robotPosHistory.push(_newPos);
        
    }
    
    checkRobotPosHistoryForStuck(pastPositions){
        var stuck = false;
        var distances = [];
        if(pastPositions.length >= this.numRobotPosHistory){
            var point1 = pastPositions[0];
            var point2 = pastPositions[pastPositions.length - 1];
            
            var dist = this.distBetweenTwoPoints(point1, point2);
            if(dist < 0.05 * scalingFactor){
                stuck = true;
            }
        }

        return stuck;
    }
    
    processInputs(headingToGoal){
        //
        this.distToGoal_p = this.distBetweenTwoPoints(this.pos, goal.pos);
        var distToGoal_m = this.distToGoal_p / scalingFactor;
//        if(this.index == 1){
//            print("robot dist to goal: " + str(distToGoal_m) + " m");
//            print("heading to goal: " + str(headingToGoal / PI) + " pi")
//        }
        this.checkVisionLinesToObstacles()
        
        let inputs = [];
        inputs[0] = distToGoal_m/100;
        inputs[1] =(headingToGoal + PI)/ (2* PI);
        inputs[2] = (this.orientation + PI)/ (2*PI);
        for(var  i = 0; i < this.numberOfVisionLines; i++){
            inputs[i + 3] = this.distanceAlongVisionLine_p[i] / (scalingFactor * 100);
        }
        
        let output = this.brain.chooseMovement(inputs);
        var magVelocity = (output[0] - 0.5) * 3;
        var headingVelocity = (output[1] - 0.5) * (2*PI);
//        if(this.index == 1){
//            print("robot heading is: " + str(headingVelocity / PI) + " pi")
//        }
        this.angularVel_rad_s = (output[2] - 0.5) * 1;
        
        this.vel_ms.x = magVelocity * cos(headingVelocity);
        this.vel_ms.y = magVelocity * sin(headingVelocity);
//        this.vel_ms.x = (output[0] - 0.5) * 2;
//        this.vel_ms.y = (output[1] - 0.5) * 2
        return headingVelocity;
        
        
    }
    
    distBetweenTwoPoints(point1, point2){
        var dist;
        var changeInX = point2.x - point1.x;
        var changeInY = point2.y - point1.y;
        dist = sqrt(sq(changeInX) + sq(changeInY));

        return dist;
    }
    
    getHeadingToGoal(robotPos, goalPos){
        var heading;
        var changeInX = goalPos.x - robotPos.x;
        var changeInY = goalPos.y - robotPos.y;
        heading = atan2(changeInY, changeInX);
        return heading;
        
    }
    
    checkRobotOrientationAndMovement(robotOrientation, robotHeading){
        var goodOrientation = true;
        var angleDiff = abs(robotHeading - robotOrientation);
        if(angleDiff >= PI){
            angleDiff = (2*PI) - angleDiff;
        }

        
        if(angleDiff > 0.5){
            goodOrientation = false;
        }
        
        
        return goodOrientation;
    }
    
    
    checkProgressionToGoal(robotHeading, world_goalHeading){
        var badProgression = false;
        var world_robotHeading = -robotHeading;
        if( world_robotHeading >= PI){
            world_robotHeading -= (2*PI);
        }
        if( world_robotHeading < -PI){
            world_robotHeading += (2*PI);
        }
        
        var headingDiff = abs(world_goalHeading - world_robotHeading);
        if(headingDiff >= PI){
            headingDiff  = (2*PI) - headingDiff;
        }

        // 30 degrees
        if (headingDiff > 0.5){
            badProgression = true;
            
        }
        
        return badProgression;
    }
    
    checkVisionLinesToObstacles(){
        // iterate over each vision line
        // this.numberOfVisionLines
        for(var i = 0; i < this.numberOfVisionLines; i++ ){
            let visionLineHitObstacle = false;
            let angle = this.startingAngle_rad + (i * (this.angleSpread_rad/ (this.numberOfVisionLines - 1))) + this.orientation;
            let visionLineIncrementer = 0;
            if(angle < -PI ){
                angle = (2*PI) + angle;
            }
            if(angle > PI){
                angle = angle - (2*PI);
            }
            // iterate over set points over each vision line and check collision with an obstacle
            while(visionLineHitObstacle == false &&  visionLineIncrementer < this.visionLineMaxDist_p){
                let newPointx = this.pos.x + (visionLineIncrementer*cos(angle));
                let newPointy = this.pos.y + (visionLineIncrementer*sin(angle));
                //print("new point x: " + str(newPointx) + " y: " + str(newPointy));
                if(this.checkCollisionWithObstacle(newPointx,newPointy) == true){
                    visionLineHitObstacle = true;
                    this.distanceAlongVisionLine_p[i] = visionLineIncrementer;
                }
                visionLineIncrementer = visionLineIncrementer + 10;
                
            }
            if(visionLineHitObstacle == false){
                this.distanceAlongVisionLine_p[i] = this.visionLineMaxDist_p;
            }
        }
    }
    
    checkCollisionWithObstacle(newx, newy){
        let goingToHitObstacle = false;
        for(var i = 0; i < world.obstacles.length; i++){
            if(newx > world.obstacles[i].xpos - (world.obstacles[i].rectWidth/2) && newx < world.obstacles[i].xpos + (world.obstacles[i].rectWidth/2) ){
                if(newy > world.obstacles[i].ypos - (world.obstacles[i].rectHeight/2) && newy < world.obstacles[i].ypos+(world.obstacles[i].rectHeight/2)){
                    goingToHitObstacle = true;
                }
            }
        }
        return goingToHitObstacle;
    }
    
    updateMovement(){
        var scaledVel = createVector(0, 0);
        var worldVel = createVector(0, 0);
        scaledVel.x = ((this.vel_ms.x * scalingFactor) / fRate);
        scaledVel.y = ((this.vel_ms.y * scalingFactor) / fRate);
        //var worldVelocities = this.convertVelRobotToWorld(scaledVel.x, scaledVel.y, this.orientation);
        
//        worldVel.x = worldVelocities[0];
//        worldVel.y = worldVelocities[1];
        worldVel.x = scaledVel.x;
        worldVel.y = scaledVel.y;
        
        
        var newPos = createVector(this.pos.x, this.pos.y);
        newPos.add(worldVel);
        if(this.checkNewRobotPosValid(newPos, this.orientation) == true){
            this.pos.add(worldVel);
            var worldAngularVel = this.angularVel_rad_s / fRate;
            this.orientation += worldAngularVel;
            if(this.orientation < -PI ){
                this.orientation = (2*PI) + this.orientation;
            }
            if(this.orientation > PI){
                this.orientation = this.orientation - (2*PI);
            }
        }
        else{
            this.crashed = true;
            this.crashedTracker[this.episodeNumber] = true;
        }
        
        
    }
    
    checkForTermination(){
        var robotFinished = false;
        if(this.checkIfRobotReachedGoal(this.pos, goal.pos, goal.size/2) == true){
            robotFinished = true;
            print("robot reached goal");
            this.goalReachedEpisode[this.episodeNumber] = true;
        }
        
        if(this.crashed == true){
            robotFinished = true;
        }
        
        return robotFinished;
    }
    
    terminateRobot(){
        this.running = false;
        
        this.storeDistToGoalForEpisode(this.distToGoalEachEpisode.length);
        this.storeDistFromOriginForEpisode(this.distFromOriginEachEpisode.length);
        this.robotAliveCounter[this.episodeNumber] = population.counter;
        this.pos.x = 10000
        
    }
    
    setStartDist(goalPos){
        var tempPos = createVector(width/2, height/2);
        var distToGoal = this.distBetweenTwoPoints(tempPos, goalPos);
        this.startDistToGoal[this.episodeNumber] = distToGoal;
    }
    
    calcFitness(){
        var fitness = 0;
        var fitnessEachEpisode = [];
        var curosityFitnessEachEpisode = [];
        for(var i = 0; i< this.distToGoalEachEpisode.length; i++){
            // Modifiers between 0 and 1 that represent how well they moved towards the goal at all stages 
            // and if they orientated themselves towards the direction theyr were moving.
            var goodGoalProgressionPortion = 1 - (this.badProgressionCounter[i] / this.robotAliveCounter[i]);
            var goodOrientationPortion = this.robotGoodOrientationCounter[i] / this.robotAliveCounter[i];
            var distFromOriginProportion = this.distFromOriginEachEpisode[i] / 25;
            
            // Need to apply some weighting to the above propertonal values to determine their effect on robot fitness
            var goalProgressWeighting = 10;
            var orientationWeighting = 10;
            var distToGoalWeighting = 20;
            var distFromOriginWeighting = 10;
            
            var sumDistPerEpisodeToGoal = 0;
            for(var j = 0; j < this.distToGoalPerCounterPerEpisode[i].length; j++){
                sumDistPerEpisodeToGoal += this.distToGoalPerCounterPerEpisode[i][j];
            }
            var avgDistToGoalThisEpisode = sumDistPerEpisodeToGoal / this.distToGoalPerCounterPerEpisode[i].length;
            
            fitnessEachEpisode[i] = distToGoalWeighting / (sq(avgDistToGoalThisEpisode) + 1);
            //fitnessEachEpisode[i] += (distFromOriginWeighting * distFromOriginProportion);
            //fitnessEachEpisode[i] += (orientationWeighting * goodOrientationPortion);
            //fitnessEachEpisode[i] += (goalProgressWeighting * goodGoalProgressionPortion);
            
            curosityFitnessEachEpisode[i] = distFromOriginProportion * distFromOriginWeighting;
            
            
            
            if(this.crashedTracker[this.episodeNumber] == true){
                fitnessEachEpisode[i] /= 2;
            }
            
            if(this.goalReachedEpisode[i] == true){
                fitnessEachEpisode[i] *= 2;
            }

        }
        var sumFit = 0;
        var sumFitCuriosity = 0;
        for(var i = 0; i< this.distToGoalEachEpisode.length; i++){
            sumFit += fitnessEachEpisode[i];
            sumFitCuriosity += curosityFitnessEachEpisode[i];
        }
        fitness = sumFit/this.distToGoalEachEpisode.length;
        this.curiosityFitness = sumFitCuriosity/this.distToGoalEachEpisode.length;
        
        this.fitness = fitness;
    }
    
    checkIfRobotInOuterRing(){
        var inOuterRing = false;
        var marginPadding = 20;
        if(this.episodeNumber == 0){
            if(this.pos.x > 650 + marginPadding){
                inOuterRing = true;
            } 
        }
        else if(this.episodeNumber == 1){
            if(this.pos.x < 250 - marginPadding){
                inOuterRing = true;
            } 
        }
        else if(this.episodeNumber == 2){
            if(this.pos.y > 650 + marginPadding){
                inOuterRing = true;
            } 
        }
        else if(this.episodeNumber == 3){
            if(this.pos.y < 250 - marginPadding){
                inOuterRing = true;
            }
        }
        
        return inOuterRing;
    }
    
    storeDistToGoalForEpisode(episodeNumber){
        this.distToGoal_p = this.distBetweenTwoPoints(this.pos, goal.pos)
        this.distToGoalEachEpisode[episodeNumber] = this.distToGoal_p / scalingFactor;
        //print("robot: " + str(this.index) + " dist: " + str(this.distToGoalEachEpisode[episodeNumber]))
    }
    
    storeDistFromOriginForEpisode(episodeNumber){
        var origin = createVector(0, 0);
        var distFromOrigin = this.distBetweenTwoPoints(this.pos, origin);
        this.distFromOriginEachEpisode[episodeNumber] = distFromOrigin / scalingFactor;
    }
    
    checkIfRobotReachedGoal(robotPos, goalPos, goalPadding){
        var goalReached = false;
        var dist = this.distBetweenTwoPoints(robotPos, goalPos);
        if( dist < goalPadding){
            goalReached = true
            this.pos.x = goal.pos.x;
            this.pos.y = goal.pos.y;
        }
        
        if((dist/scalingFactor) < this.robotClosestDist[this.episodeNumber]){
            this.robotClosestDist[this.episodeNumber] = dist/scalingFactor;
        }
        return goalReached;
    }
    
    checkIfRobotReachedAllGoals(numEpisodes){
        var robotReachedAll = true;
        for(var i = 0 ; i < numEpisodes; i++){
            if(this.goalReachedEpisode[i] == false){
                robotReachedAll = false;
            }
        }
        
        return robotReachedAll;
    }
    
    
    
    
    checkNewRobotPosValid(newPos, currentOrientation){
        var newPosValid = true;
        var cornerPoints = this.getRobotCornerPoints();
        for(var i =0; i<world.obstacles.length; i++){
            if(world.obstacles[i].checkRobotCollisionWithObstacle(cornerPoints) == true){
                newPosValid = false;
            }
        }
        
        
        return newPosValid;
    }
    
    getRobotCornerPoints(){
        var cornerPoints = [];
        // top left

        var topLeftPoint = createVector(this.length/2, -this.width/2);
        var botLeftPoint = createVector(-this.length/2, -this.width/2);
        var botRightPoint = createVector(-this.length/2, this.width/2);
        var topRightPoint = createVector(this.length/2, this.width/2);
        
        //var angle = PI/2;
        
        this.tempRobotCornerPoints[0] = createVector(0,0);
        this.tempRobotCornerPoints[0].x = cos(this.orientation) * topLeftPoint.x - (sin(this.orientation) * topLeftPoint.y);
        this.tempRobotCornerPoints[0].x += this.pos.x
        this.tempRobotCornerPoints[0].y = (sin(this.orientation) * topLeftPoint.x) + (cos(this.orientation) * topLeftPoint.y);
        this.tempRobotCornerPoints[0].y += this.pos.y
        
        //bottom left
        this.tempRobotCornerPoints[1] = createVector(0,0);
        this.tempRobotCornerPoints[1].x = cos(this.orientation) * botLeftPoint.x - (sin(this.orientation) * botLeftPoint.y);
        this.tempRobotCornerPoints[1].x += this.pos.x
        this.tempRobotCornerPoints[1].y = (sin(this.orientation) * botLeftPoint.x) + (cos(this.orientation) * botLeftPoint.y);
        this.tempRobotCornerPoints[1].y += this.pos.y
        
        // bottom right
        this.tempRobotCornerPoints[2] = createVector(0,0);
        this.tempRobotCornerPoints[2].x = cos(this.orientation) * botRightPoint.x - (sin(this.orientation) * botRightPoint.y);
        this.tempRobotCornerPoints[2].x += this.pos.x
        this.tempRobotCornerPoints[2].y = (sin(this.orientation) * botRightPoint.x) + (cos(this.orientation) * botRightPoint.y);
        this.tempRobotCornerPoints[2].y += this.pos.y
        
        // top right
        this.tempRobotCornerPoints[3] = createVector(0,0);
        this.tempRobotCornerPoints[3].x = cos(this.orientation) * topRightPoint.x - (sin(this.orientation) * topRightPoint.y);
        this.tempRobotCornerPoints[3].x += this.pos.x
        this.tempRobotCornerPoints[3].y = (sin(this.orientation) * topRightPoint.x) + (cos(this.orientation) * topRightPoint.y);
        this.tempRobotCornerPoints[3].y += this.pos.y
        
        for(var i = 0; i < this.tempRobotCornerPoints.length; i++){
        
            cornerPoints[i] = this.tempRobotCornerPoints[i];
        }
        
        return cornerPoints;
    }
    
    convertVelRobotToWorld(robotX, robotY, rotZ){
        var worldX = cos(rotZ) * robotX - (sin(rotZ) * robotY);
        var worldY = (sin(rotZ) * robotX) + (cos(rotZ) * robotY);
        
        
        return [worldX, worldY];
    }
    
    show(){
        push();
        //noStroke();
        fill(0, 0, 255);
//        ellipseMode(CENTER)
//        for(var i = 0; i < this.tempRobotCornerPoints.length; i++){
//            ellipse(this.tempRobotCornerPoints[i].x, this.tempRobotCornerPoints[i].y, 5, 5);
//        }
        translate(this.pos.x, this.pos.y);
        rotate(this.orientation);
        //this.drawVisionLines();
        this.drawRobot();


        pop();
        
        
        
    }
    
    drawRobot(){
        if(this.bestRobotLastGen == true){
            fill(0, 0, 255, this.drawAlpha);
        }
        else if(this.curiousRobot == true){
            fill(0, 255, 0, this.drawAlpha);
        }
        else{
            fill(255, 255, 255, this.drawAlpha);
        }
        
        
        noStroke();
        rectMode(CENTER);
        rect(0, 0, this.width, this.length);
        fill(255, 0, 100, this.drawAlpha);
        ellipse(this.length/2 - 5, 0, 8, 8);
        
        
        
    }
    
    drawVisionLines(){
        for( var i = 0; i < this.numberOfVisionLines; i++){
            let angle = this.startingAngle_rad + (i * (this.angleSpread_rad/ (this.numberOfVisionLines - 1)))
            if(this.distanceAlongVisionLine_p[i] < this.visionLineMaxDist_p){
                stroke(255, 0, 0, this.drawAlpha);
            }
            else{
                stroke(255);
            }
            
            line(0, 0, this.distanceAlongVisionLine_p[i]*Math.cos(angle), this.distanceAlongVisionLine_p[i]*Math.sin(angle))
        }
        
    }
    
}
    
    