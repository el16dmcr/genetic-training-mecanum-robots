class Population{
    constructor(){
        this.robots = [];
        this.popSize = 50;
        this.matingPool = [];
        this.robotWidth = 0.6;
        this.robotLength = 0.544;
        this.visionLines = 7;
        this.countsPerEpisode = 1000;
        this.counter = 0
        this.episodeCounter = 0;
        this.episodesPerGeneration = 4;
        
        this.worstFitnessEachGen = [];
        this.meanFitnessEachGen = [];
        this.bestFitnessEachGen = [];
        
        this.worstFitnessRollingAverage = [];
        this.meanFitnessRollingAverage = [];
        this.bestFitnessRollingAverage = [];
        
        this.numGoalsReachedPerGen = [];
        
        this.bestFitnessAllTime = 0;
        
        this.portionNormalFitnessEval = 1;
        
        this.minMutationSD = 0.01;
        this.maxMutationSD = 0.3;
        
        this.minMutateChance = 0.01;
        this.maxMutateChanceLow = 0.2;
        this.maxMutateChanceHigh = 0.4;
        
        
        for(var i = 0; i <this.popSize; i++){
            this.robots[i] = new Robot(this.robotWidth, this.robotLength, this.visionLines, i, false, false, this.countsPerEpisode, false);
            this.robots[i].setStartDist(goal.pos)
        }
        
        this.useJustMutation = true;
        this.counterAfterSpawnObstacles = 0;
    }
    
    run(){
        this.counter++;
        for(var i = 0; i < this.popSize; i++){
            this.robots[i].handleRobot();
            //this.robots[i].testFunctionHandleRobot();
            
        }
    }
    
    display(){
        for(var i = 0; i < this.popSize; i++){
            this.robots[i].show(goal.pos);
        }
    }
    
    handleNextGeneration(){
        this.episodeCounter = 0;
        var bestRobotIndex = this.evaluate();
        this.naturalSelection(bestRobotIndex, this.matingPool);
        for(var i = 0; i < this.popSize; i++){
            this.robots[i].setStartDist(goal.pos);
        }
        
    }
    

    
    evaluate()
    {
        // evalueat any robots that hadnt finished running yet
        
        for(var i = 0; i< this.robots.length; i++){
            // if the robot was running till the very end 
            this.robots[i].calcFitness();
                
        }
        
        var bestFitness = 0;
        var sumFitness = 0;
        var sumFitnessCuriosity = 0;
        var bestRobotIndex = 0;
        
        
        
        for(var i = 0; i< this.robots.length; i++){
            sumFitness += this.robots[i].fitness;
            sumFitnessCuriosity += this.robots[i].curiosityFitness;
            if(this.robots[i].fitness > bestFitness){
                bestFitness = this.robots[i].fitness;
                bestRobotIndex = i;
            }
        }
        
        print("best fitness this gen: " + str(bestFitness));
        this.bestFitnessEachGen.push(bestFitness);
        this.meanFitnessEachGen.push(sumFitness / this.popSize);
        this.worstFitnessEachGen.push(this.returnWorstFitnessThisGen());
        this.calcAverageFitness();
        this.calculateNumbGoalsReached();
        
        if(spawnObstacles == false){
            if(sumFitness / this.popSize >= (0.675 * theoreticalMaxFitness)){
                spawnObstacles = true;
            }
        }
        else{
            if(this.useJustMutation == false){
                this.counterAfterSpawnObstacles += 1;
                if(this.counterAfterSpawnObstacles > 800){
                    this.useJustMutation = true;
                }
            }
        }
        
        if(bestFitness > this.bestFitnessAllTime){
            this.bestFitnessAllTime = bestFitness;
        }
        
        for(var i = 0; i < this.robots.length; i++){
            this.robots[i].normalisedFitness = this.robots[i].fitness /sumFitness;
            this.robots[i].normalisedCuriosityFitness = this.robots[i].curiosityFitness / sumFitnessCuriosity;
        }
        
        this.matingPool = [];
        this.curiosityTracker = [];
        // natural selection purely mutation, therefore 1 parent to 1 child
        
//        for(var parent = 0; parent < this.popSize - 1; parent++){
//            var index = this.pickOneBrainWithPriority();
//            this.matingPool.push(index);
//        }
        var normalisedFitnesses = [];
        var normalisedFitnessCuriosity = [];
        for(var robot = 0; robot < this.popSize; robot++){
            normalisedFitnesses[robot] = this.robots[robot].normalisedFitness;
            normalisedFitnessCuriosity[robot] = this.robots[robot].normalisedCuriosityFitness;
        }
        
        var parentsFromNormalFitness;
        if(this.useJustMutation == false){
            parentsFromNormalFitness = 2 * (this.portionNormalFitnessEval * this.popSize);
        }
        else{
            parentsFromNormalFitness = 1 * (this.portionNormalFitnessEval * this.popSize);
        }
        for(var parent = 0; parent < parentsFromNormalFitness; parent++){
            var index = this.pickOneBrainWithPriority(normalisedFitnesses);
            this.matingPool.push(index);
            this.curiosityTracker.push(false);
        }
        
//        var parentsFromCuriosity = (2 * this.popSize) - 2 - parentsFromNormalFitness;
//        print("parents curiosuty: " + str(parentsFromCuriosity));
//        for(var parent = 0; parent < parentsFromCuriosity; parent++){
//            var index = this.pickOneBrainWithPriority(normalisedFitnessCuriosity);
//            this.matingPool.push(index);
//            this.curiosityTracker.push(true);
//        }
        
        return bestRobotIndex;
    }
    
    checkIfAnyRobotsReachedAllGoals(){
        var anyRobotsSuccesful = false;
        for(var i = 0; i < this.popSize; i++){
            if(this.robots[i].checkIfRobotReachedAllGoals(this.episodesPerGeneration) == true){
                anyRobotsSuccesful = true;
            }
        }
        return anyRobotsSuccesful;
    }
    
    returnWorstFitnessThisGen(){
        var worstFitness = 100000
        for(var i = 0; i < this.popSize; i++){
            if(this.robots[i].fitness < worstFitness){
                worstFitness = this.robots[i].fitness;
            }
        }
        return worstFitness;
    }
    
    
    pickOneBrainWithPriority(normalisedFitnesses){
        var index = 0;
        var r = random(1);
        while( r > 0){
            r = r - normalisedFitnesses[index];
            index = index + 1;
        }
        
        return index - 1;
    }

    
    naturalSelection(bestRobotIndex, matingPool){
        var newRobots = [];
        //print("mating pool is: " + str(matingPool));
        print("mating pool length: " + str(matingPool.length))
        
        if(this.useJustMutation == false){
            newRobots = this.crossOverWeightMixing(matingPool, bestRobotIndex);
        }
        else{
            newRobots = this.crossOverPurelyMutation(matingPool, bestRobotIndex);
        }
        
        
        for(var robotIndex = 0; robotIndex < this.popSize; robotIndex++){
            this.robots[robotIndex].brain.dispose();
            this.robots[robotIndex] = newRobots[robotIndex];
        }
        

    }
    
    crossOverWeightMixing(matingPool, bestRobotIndex){
        var numParentsInPool = matingPool.length;
        
        var parent1;
        var parent2;
        var fitness1;
        var fitness2;
        var randomIndex;
        var childBrain;
        var newRobots = [];
        
        for(var robotIndex = 0; robotIndex < this.popSize; robotIndex++){
            var curiosityBorn = false;
            if(robotIndex == 0){
                // Elitism, keep the best robot, completely unmodified
                parent1 = this.robots[bestRobotIndex].brain;
                print(this.robots[bestRobotIndex]);
            }
            else{
                randomIndex = Math.floor(random(numParentsInPool));
                if(this.curiosityTracker[randomIndex] == true){
                    curiosityBorn = true;
                }
                this.curiosityTracker.splice(randomIndex, 1);
                parent1 = this.robots[matingPool[randomIndex]].brain;
                fitness1 = this.robots[matingPool[randomIndex]].fitness;
                matingPool.splice(randomIndex, 1);
                numParentsInPool--;
                
                randomIndex = Math.floor(random(numParentsInPool));
                if(this.curiosityTracker[randomIndex] == true){
                    curiosityBorn = true;
                }
                this.curiosityTracker.splice(randomIndex, 1);
                parent2 = this.robots[matingPool[randomIndex]].brain;
                fitness2 = this.robots[matingPool[randomIndex]].fitness;
                matingPool.splice(randomIndex, 1);
                numParentsInPool--;
            }
            
            
            if(robotIndex != 0){
                
                childBrain = parent1.crossover(parent2);
                var avgFitness = (fitness1 + fitness2) / 2;
                var mutationSD = map(avgFitness, theoreticalMaxFitness, 0, this.minMutationSD, this.maxMutationSD, true)
                var lowMutateChance = map(avgFitness, theoreticalMaxFitness, 0, this.minMutateChance, this.maxMutateChanceLow, true);
                var highMutateChance = map(avgFitness, theoreticalMaxFitness, 0, this.minMutateChance, this.maxMutateChanceHigh, true);
                if(robotIndex < 20){
                    childBrain.mutate(lowMutateChance, mutationSD)
                }
                else{
                    childBrain.mutate(highMutateChance, mutationSD)
                }
                newRobots.push(new Robot(this.robotWidth, this.robotLength, this.visionLines, robotIndex, false, curiosityBorn, this.countsPerEpisode, childBrain));
                childBrain.dispose();
            }
            else{
                // Keep this childbrain the exact same
                childBrain = parent1.copy();
                newRobots.push(new Robot(this.robotWidth, this.robotLength, this.visionLines, robotIndex, true, curiosityBorn, this.countsPerEpisode, childBrain));
                childBrain.dispose();
            }
        }
        return newRobots;
    }
    
    crossOverPurelyMutation(matingPool, bestRobotIndex){
        var numParentsInPool = matingPool.length;
        
        var parent;
        var randomIndex;
        var childBrain;
        var newRobots = [];
        var parentFitness = 0;
        
        for(var robotIndex = 0; robotIndex < this.popSize; robotIndex++){
            if(robotIndex == 0){
                // Elitism, keep the best robot, completely unmodified
                parent = this.robots[bestRobotIndex].brain;
                print(this.robots[bestRobotIndex])
            }
            else{
                randomIndex = Math.floor(random(numParentsInPool));
                parent = this.robots[matingPool[randomIndex]].brain;
                parentFitness = this.robots[matingPool[randomIndex]].fitness;
                matingPool.splice(randomIndex, 1);
                numParentsInPool--;
            }
            
            var mutationSD = map(parentFitness, theoreticalMaxFitness, 0, this.minMutationSD, this.maxMutationSD, true)
            var lowMutateChance = map(parentFitness, theoreticalMaxFitness, 0, this.minMutateChance, this.maxMutateChanceLow, true);
            var highMutateChance = map(parentFitness, theoreticalMaxFitness, 0, this.minMutateChance, this.maxMutateChanceHigh, true);
            if(robotIndex != 0){
                if(robotIndex < 30){
                    childBrain = parent.copy();
                    childBrain.mutate(lowMutateChance, mutationSD)
                }
                else{
                    childBrain = parent.copy();
                    childBrain.mutate(highMutateChance, mutationSD)
                }
                newRobots.push(new Robot(this.robotWidth, this.robotLength, this.visionLines, robotIndex, false, false, this.countsPerEpisode, childBrain));
                childBrain.dispose();
            }
            else{
                // Keep this childbrain the exact same
                childBrain = parent.copy();
                newRobots.push(new Robot(this.robotWidth, this.robotLength, this.visionLines, robotIndex, true, false, this.countsPerEpisode, childBrain));
                childBrain.dispose();
            }
        }
        return newRobots;
        
    }
    
    
    checkIfGenerationComplete(){
        var genComplete = false
        if(this.checkIfEpisodeComplete() == true){
            this.episodeCounter++;
            
            for(var i = 0; i< this.robots.length; i++){
                if(this.robots[i].running == true){
                // if the robot was running till the very end 
                    this.robots[i].terminateRobot();
                
                }
            }
            this.counter = 0;
            if(this.episodeCounter >= this.episodesPerGeneration){
                genComplete = true;
            }
            else{
                this.newEpisode();
            }
        }

        
        return genComplete;
    }
    
    checkIfEpisodeComplete(){
        var episodeComplete = false;
        if(this.counter >= this.countsPerEpisode){
            episodeComplete = true;
            return episodeComplete;
        }
        
        var allRobotsComplete = true;
        for(var i = 0; i < this.robots.length; i++){
            if(this.robots[i].running == true){
                allRobotsComplete = false;
            }
        }
        
        if(allRobotsComplete == true){
            episodeComplete = true;
        }
        
        
        return episodeComplete;
    }
    
    newEpisode(){
        for(var i = 0; i<this.popSize; i++){
            this.robots[i].resetRobotForEpisode();
            
        }
        goal.incrementGoalQuadIndex();
        goal.newRandomLocation();
        for(var i = 0; i < this.popSize; i++){
            this.robots[i].setStartDist(goal.pos);
        }
        

    }
    
    calcAverageFitness(){
        var loopNumber = 7;
        if(this.bestFitnessEachGen.length < 7){
            loopNumber = this.bestFitnessEachGen.length;
        }
        
        var sumWorstFitness = 0;
        var sumMeanFitness = 0;
        var sumBestFitness = 0;
        for(var i = 0; i < loopNumber; i++){
            sumWorstFitness += this.worstFitnessEachGen[this.worstFitnessEachGen.length - loopNumber + i];
            sumMeanFitness += this.meanFitnessEachGen[this.meanFitnessEachGen.length - loopNumber + i];
            sumBestFitness += this.bestFitnessEachGen[this.meanFitnessEachGen.length - loopNumber + i];
        }
        this.worstFitnessRollingAverage.push(sumWorstFitness / loopNumber);
        this.meanFitnessRollingAverage.push(sumMeanFitness / loopNumber);
        this.bestFitnessRollingAverage.push(sumBestFitness / loopNumber);
        print(this.worstFitnessRollingAverage);
    }
    
    calculateNumbGoalsReached(){
        var numGoalsReached = 0;
        for(var  i = 0; i < this.popSize; i++){
            for(var j = 0; j < this.episodesPerGeneration; j++){
                if(this.robots[i].goalReachedEpisode[j] == true){
                    numGoalsReached += 1;
                }
            }
        }
        this.numGoalsReachedPerGen.push(numGoalsReached);
    }
    
}