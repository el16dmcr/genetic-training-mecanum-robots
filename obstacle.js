class Obstacle{
    
    constructor(_xpos_m, _ypos_m, length_m, width_m, vertical){
        this.xpos = _xpos_m * scalingFactor;
        this.ypos = _ypos_m * scalingFactor;
        //print("x pos: " + str(this.xpos) + " y pos: " + str(this.ypos));

        
        if( vertical == true ){
            this.rectWidth = width_m * scalingFactor;
            this.rectHeight = length_m * scalingFactor;
        }
        else{
            this.rectWidth = length_m * scalingFactor;
            this.rectHeight = width_m * scalingFactor;
        } 
    }
    
    
    show(){
        push();
        fill(255, 255, 255);
        rectMode(CENTER);
        rect(this.xpos, this.ypos, this.rectWidth, this.rectHeight);
        pop();
    }
    
    checkCollisionWithObstacle(newx, newy, obstaclePadding, radiusOfObject){
        let collision = false;
        if(newx + radiusOfObject > this.xpos - obstaclePadding - (this.rectWidth/2) && newx - radiusOfObject < this.xpos + (this.rectWidth/2) + obstaclePadding){
            if(newy + radiusOfObject > this.ypos - obstaclePadding - (this.rectHeight/2) && newy - radiusOfObject< this.ypos + (this.rectHeight/2) + obstaclePadding){
                collision = true;
            }
        }
        return collision;
    }
    
    checkRobotCollisionWithObstacle(robotCornerPoints){
        let collision = false;
        var obstaclePoints = [];
        obstaclePoints[0] = createVector(this.xpos - this.rectWidth/2, this.ypos - this.rectHeight/2);
        obstaclePoints[1] = createVector(this.xpos - this.rectWidth/2, this.ypos + this.rectHeight/2);
        obstaclePoints[2] = createVector(this.xpos + this.rectWidth/2, this.ypos + this.rectHeight/2);
        obstaclePoints[3] = createVector(this.xpos + this.rectWidth/2, this.ypos - this.rectHeight/2);
        var n = 4;
        for(var cornerPoint = 0; cornerPoint< robotCornerPoints.length; cornerPoint++){
            var point = robotCornerPoints[cornerPoint];
            if(this.isInside(obstaclePoints, n, point) == true){
                collision = true;
                return collision;
            }
        }
        
        
        
        return collision;
    }
    
    // Given three colinear points p, q, r, 
    // the function checks if point q lies 
    // on line segment 'pr' 
    onSegment(p, q, r) 
    { 
        if (q.x <= max(p.x, r.x) && 
            q.x >= min(p.x, r.x) && 
            q.y <= max(p.y, r.y) && 
            q.y >= min(p.y, r.y)) 
        { 
            return true; 
        } 
        return false; 
    } 
 
    // To find orientation of ordered triplet (p, q, r). 
    // The function returns following values 
    // 0 --> p, q and r are colinear 
    // 1 --> Clockwise 
    // 2 --> Counterclockwise 
    orientation(p, q, r) 
    { 
        var val = (q.y - p.y) * (r.x - q.x) 
                - (q.x - p.x) * (r.y - q.y); 
 
        if (val == 0) 
        { 
            return 0; // colinear 
        } 
        return (val > 0) ? 1 : 2; // clock or counterclock wise 
    } 
 
    // The function that returns true if 
    // line segment 'p1q1' and 'p2q2' intersect. 
    doIntersect(p1, q1, p2, q2) 
    { 
        // Find the four orientations needed for 
        // general and special cases 
        var o1 = this.orientation(p1, q1, p2); 
        var o2 = this.orientation(p1, q1, q2); 
        var o3 = this.orientation(p2, q2, p1); 
        var o4 = this.orientation(p2, q2, q1); 
 
        // General case 
        if (o1 != o2 && o3 != o4) 
        { 
            return true; 
        } 
 
        // Special Cases 
        // p1, q1 and p2 are colinear and 
        // p2 lies on segment p1q1 
        if (o1 == 0 && this.onSegment(p1, p2, q1)) 
        { 
            return true; 
        } 
 
        // p1, q1 and p2 are colinear and 
        // q2 lies on segment p1q1 
        if (o2 == 0 && this.onSegment(p1, q2, q1)) 
        { 
            return true; 
        } 
 
        // p2, q2 and p1 are colinear and 
        // p1 lies on segment p2q2 
        if (o3 == 0 && this.onSegment(p2, p1, q2)) 
        { 
            return true; 
        } 
 
        // p2, q2 and q1 are colinear and 
        // q1 lies on segment p2q2 
        if (o4 == 0 && onSegment(p2, q1, q2)) 
        { 
            return true; 
        } 
 
        // Doesn't fall in any of the above cases 
        return false; 
    } 
 
    // Returns true if the point p lies 
    // inside the polygon[] with n vertices 
    isInside(polygon, n, p) 
    { 
        // There must be at least 3 vertices in polygon[] 
        if (n < 3) 
        { 
            return false; 
        } 
 
        // Create a point for line segment from p to infinite 
        var INF = 1000
        var extreme = createVector(INF, p.y); 
 
        // Count intersections of the above line 
        // with sides of polygon 
        var count = 0, i = 0; 
        do
        { 
            var next = (i + 1) % n; 
 
            // Check if the line segment from 'p' to 
            // 'extreme' intersects with the line 
            // segment from 'polygon[i]' to 'polygon[next]' 
            if (this.doIntersect(polygon[i], polygon[next], p, extreme)) 
            { 
                // If the point 'p' is colinear with line 
                // segment 'i-next', then check if it lies 
                // on segment. If it lies, return true, otherwise false 
                if (this.orientation(polygon[i], p, polygon[next]) == 0) 
                { 
                    return this.onSegment(polygon[i], p, polygon[next]); 
                } 
 
                count++; 
            } 
            i = next; 
        } while (i != 0); 
        
 
        // Return true if count is odd, false otherwise 
        return (count % 2 == 1); // Same as (count%2 == 1) 
    } 
 
    // Driver Code 
    testFunction() 
    { 

        var testPolygon1 = [];
        testPolygon1[0] = createVector(0,0);
        testPolygon1[1] = createVector(10,0);
        testPolygon1[2] = createVector(10,10);
        testPolygon1[3] = createVector(0,10);
        var n = testPolygon1.length; 
        var p = createVector(20, 20); 
        if (this.isInside(testPolygon1, n, p)) 
        { 
            print("function returned yes, FALSE");
        } 
        else
        { 
            print("function returned no, CORRECT")
        } 
        p = createVector(5, 5);
        if (this.isInside(testPolygon1, n, p)) 
        { 
            print("function returned yes, CORRECTS"); 
        } 
        else
        { 
            print("function returned NO, FALSE");
        } 
//        Point polygon2[] = {new Point(0, 0), 
//            new Point(5, 5), new Point(5, 0)}; 
//        p = new Point(3, 3); 
//        n = polygon2.length; 
//        if (isInside(polygon2, n, p)) 
//        { 
//            System.out.println("Yes"); 
//        } 
//        else
//        { 
//            System.out.println("No"); 
//        } 
//        p = new Point(5, 1); 
//        if (isInside(polygon2, n, p)) 
//        { 
//            System.out.println("Yes"); 
//        } 
//        else
//        { 
//            System.out.println("No"); 
//        } 
//        p = new Point(8, 1); 
//        if (isInside(polygon2, n, p)) 
//        { 
//            System.out.println("Yes"); 
//        } 
//        else
//        { 
//            System.out.println("No"); 
//        } 
//        Point polygon3[] = {new Point(0, 0), 
//                            new Point(10, 0), 
//                            new Point(10, 10), 
//                            new Point(0, 10)}; 
//        p = new Point(-1, 10); 
//        n = polygon3.length; 
//        if (isInside(polygon3, n, p)) 
//        { 
//            System.out.println("Yes"); 
//        } 
//        else
//        { 
//            System.out.println("No"); 
//        } 
    } 
} 
