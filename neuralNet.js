class NeuralNet{
    constructor(a, b, c, d, e){
        
        if(a instanceof tf.Sequential){
            this.model = a;
            this.inputNodes = b;
            this.hiddenNodes1 = c;
            this.hiddenNodes2 = d;
            this.outputNodes = e;
        }
        else{
            this.inputNodes = a;
            this.hiddenNodes1 = b;
            this.hiddenNodes2 = c
            this.outputNodes = d;
            this.model = this.createModel();
        }
        
    }
    
    dispose(){
        this.model.dispose();
    }
    
    copy(){
        return tf.tidy(() => {
            const modelCopy = this.createModel();
            const weights = this.model.getWeights();
            const weightCopies = [];
            for( var i = 0; i < weights.length; i++){
                weightCopies[i] = weights[i].clone();
            }
            modelCopy.setWeights(weightCopies);
            return new NeuralNet(modelCopy, this.inputNodes, this.hiddenNodes1, this.hiddenNodes2, this.outputNodes);
        });
        
    }
    
    mutate(chance, std_dev){
        tf.tidy(() => {
            const weights = this.model.getWeights();
            const mutatedWeights = [];
            for(var i = 0; i < weights.length; i++){
                let tensor = weights[i];
                let shape = weights[i].shape;
                let values = tensor.dataSync().slice();
                for(var j = 0 ; j<values.length; j++){
                    if(random(1) < chance){
                        let w = values[j];
                        values[j] = w + randomGaussian(0, std_dev);
                        
                    }
                    
                }
                let newTensor = tf.tensor(values, shape);
                mutatedWeights[i] = newTensor;
            }
            this.model.setWeights(mutatedWeights);
        });
    }
    
    crossover(partner){
        return tf.tidy(() => {
            const weights1 = this.model.getWeights();
            const weights2 = partner.model.getWeights();
            const childWeights = [];
            const newModel = this.createModel();
            for(var i = 0; i < weights1.length; i++){
                let tensor1 = weights1[i];
                let tensor2 = weights2[i];
                let shape1 = weights1[i].shape;
                let shape2 = weights2[i].shape;
                let values1 = tensor1.dataSync().slice();
                let values2 = tensor2.dataSync().slice();
                let newValues = [];
                for(var j = 0 ; j < values1.length; j++){
                    
                    newValues[j] = (values1[j] + values2[j])/2;
                }
                let newTensor = tf.tensor(newValues, shape1);
                childWeights[i] = newTensor;
            }
            newModel.setWeights(childWeights);
            return new NeuralNet(newModel, this.inputNodes, this.hiddenNodes1, this.hiddenNodes2, this.outputNodes);
        });
    }
    
    chooseMovement(inputs){
        // xs are the inputs into the neural net, 
        const xs = tf.tensor2d([inputs]);
        const ys = this.model.predict(xs);
        xs.dispose();
        const outputs = ys.dataSync();
        ys.dispose();
        return outputs;
        // ys are the outputs, linear and angular acceleration
    }
    
    createModel(){
        // mess around with the creation of the nuerla net for the mice
        const model = tf.sequential();
        const hidden1 = tf.layers.dense({
            units: this.hiddenNodes1,
            inputShape: [this.inputNodes],
            activation: 'tanh'
        })
        model.add(hidden1);
        const hidden2 = tf.layers.dense({
            units: this.hiddenNodes2,
            activation: 'tanh'
        })
        model.add(hidden2);
        const output = tf.layers.dense({
            units: this.outputNodes,
            activation: 'sigmoid'
        })
        model.add(output);
        return model;
    }
}